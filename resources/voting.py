# -*- coding: utf-8 -*-
#
# Copyright 2021, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=too-many-ancestors
"""
Voting logic for resource galleries
"""

from collections import defaultdict, OrderedDict
from py3votecore.stv import STV

ordinal = lambda n: "%d%s" % (n,"tsnrhtdd"[(n//10%10!=1)*(n%10<4)*n%10::4])

class RankDict(defaultdict):
    def __init__(self):
        super().__init__()
        self._max = 0

    def __missing__(self, key):
        if not isinstance(key, int):
            raise TypeError("Key must be integer")
        self._max = max([key, self._max])
        self[key] = {'items': [], 'count': 0, 'name': ordinal(key+1)}
        return self[key]

def json_stv(gallery):
    """Returns a standard set of raw data to be fed to the stv system"""
    candidates = {}
    pre_ballots = defaultdict(list)
    vote_ranks = RankDict()

    for vote in gallery.votes.order_by('order'):
        pre_ballots[vote.voter_id].append(vote.resource_id)
        candidates[vote.resource_id] = vote.resource
        vote_ranks[vote.order]['count'] += 1

    ballots = defaultdict(int)
    for lst in pre_ballots.values():
        ballots[tuple(lst)] += 1

    return dict(
        stv=[{"count": count, "ballot": list(ballot)} for ballot, count in ballots.items()],
        candidates=candidates,
        ranks=vote_ranks,
    )

def gallery_count(gallery, winners=5):
    """Calculate a simple count based vote"""
    result = defaultdict(int)
    for vote in gallery.votes.filter(order=0):
        result[vote.resource] += 1
    return sorted(list(result.items()), key=lambda item: result[item])

def gallery_stv(gallery, winners=5):
    """Calculate the STV winners of a gallery contest"""

    data = json_stv(gallery)
    candidates = data['candidates']
    try:
        result = STV(data['stv'], required_winners=winners).as_dict()
    except Exception as err:
        if "Not enough" in str(err):
            return {}
        raise

    # Re-map the primary-keys back to objects for display
    for name in ('candidates', 'tie_breaker', 'winners', 'remaining_candidates'):
        if name in result:
            result[name] = [candidates[pk] for pk in result[name]]

    # Compress rounds for display
    last_round = None
    for x, _round in enumerate(result['rounds']):
        _round['number'] = x + 1
        _round['compressed'] = 0
        _round['tallies'] = OrderedDict(sorted(_round['tallies'].items(),\
            reverse=True, key=lambda a: a[1]))
        _round['losers'] = {_round['loser']} if 'loser' in _round else {}
        # Unpack a round that selected a winner, or the first round.
        if _round.get('winners', None) or x == 0:
            last_round = None
        elif last_round:
            _round['skip'] = True
            last_round['losers'] |= _round['losers']
            last_round['compressed'] += 1
            for pk, tally in _round['tallies'].items():
                last_round['tallies'][pk] += tally
        else:
            last_round = _round

    # Re-map rounds data
    for _round in result['rounds']:
        _round['tallies'] = [(candidates[pk], tally) for pk, tally in _round['tallies'].items()]
        _round['winners'] = [candidates[pk] for pk in _round.get('winners', [])]
        _round['tied_losers'] = [candidates[pk] for pk in _round.get('tied_losers', [])]
        _round['losers'] = [candidates[pk] for pk in _round.get('losers', [])]

    result['stv'] = data['stv']
    result['vote_ranks'] = data['ranks'].values()
    return result
