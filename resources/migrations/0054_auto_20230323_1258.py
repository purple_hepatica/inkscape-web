# Generated by Django 2.2 on 2023-03-23 12:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
        ('resources', '0053_gallery_contest_per_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallery',
            name='contest_group',
            field=models.ForeignKey(blank=True, help_text='The group of people who will be able to vote in this contest. Set to none for everybody.', null=True, on_delete=django.db.models.deletion.SET_NULL, to='auth.Group'),
        ),
        migrations.AlterField(
            model_name='gallery',
            name='contest_per_user',
            field=models.PositiveIntegerField(default=0, help_text='Maximum number of contest entries per person'),
        ),
        migrations.AlterField(
            model_name='gallery',
            name='group',
            field=models.ForeignKey(blank=True, help_text='The owning group, who can add items and will have breadcrumbs to the team.', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='galleries', to='auth.Group'),
        ),
    ]
