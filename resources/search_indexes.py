#
# Copyright 2014, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Define how resources can be searched"""

from haystack.indexes import SearchIndex, Indexable, \
    DateTimeField, BooleanField, CharField, IntegerField, BooleanField,\
    MultiValueField

from .models import Resource
from .views import ResourceList
from .search_base import many_to_many

class ResourceIndex(SearchIndex, Indexable):
    """Main index for each resource"""
    text = CharField(document=True, use_template=True)
    edited = DateTimeField(model_attr='edited', null=True)
    created = DateTimeField(model_attr='created', null=True)
    published = BooleanField(model_attr='published')

    category = CharField(model_attr='category', null=True)
    license = CharField(model_attr='license', null=True)
    username = CharField()
    galleries = MultiValueField()
    tags = MultiValueField()

    liked = IntegerField(model_attr='liked')
    viewed = IntegerField(model_attr='viewed')
    downed = IntegerField(model_attr='downed')

    verified = BooleanField(model_attr='verified')
    checked_by_id = IntegerField(model_attr='checked_by_id', null=True)
    extra_status = IntegerField(model_attr='extra_status', null=True)

    def get_model(self):
        return Resource

    def get_updated_field(self):
        return 'edited'

    def prepare_username(self, obj):
        return obj.user.username

    prepare_galleries = many_to_many('galleries')
    prepare_tags = many_to_many('tags')

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        # This would need changing if we used the signal updater
        return self.get_model().objects.filter(published=True, is_removed=False).exclude(category__slug='private')

