#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Calendars for teams
"""

import sys

from dateutil import tz
from datetime import timedelta
from django.utils.translation import ugettext_lazy as _
from django.db.models import (
    Model, Manager, QuerySet, TextField, CharField, URLField, BooleanField,
    DateTimeField, ForeignKey, SET_NULL, CASCADE
)
from django.utils.timezone import now
from django.dispatch import receiver

from django.urls import reverse
from django.core.validators import MaxLengthValidator
from recurrence.fields import RecurrenceField

from person.models import Team, User

from .timezones import DST_CHOICES

UTC = tz.gettz("UTC")

class Occurance:
    """Simple holder for one occurance of an event"""
    def __init__(self, start, end):
        self.start = self.as_utc(start)
        self.end = self.as_utc(end)

    def __bool__(self):
        return self.start and (not self.end or self.start <= self.end)

    def as_utc(self, dt):
        if dt is not None:
            return dt.astimezone(UTC)
        return None

class EventQuerySet(QuerySet):
    breadcrumb_name = lambda self: _('Calendar')
    def breadcrumb_parent(self):
        return self._hints.get('instance', None)

    def get_absolute_url(self):
        team = self.breadcrumb_parent()
        if not team:
            return reverse('calendars:full')
        return reverse('team_calendar', kwargs={'team': team.slug})

    def get_ical_url(self):
        team = self.breadcrumb_parent()
        if not team:
            return reverse('calendars:full_feed')
        return reverse('team_calendar_feed', kwargs={'team': team.slug})

    def future_events(self):
        """Return just the future events"""
        events = [(ev, ev.next_occurance()) for ev in self]
        return sorted([r for r in events if r[1]], key=lambda r: r[1].start)

    def past_events(self):
        """Return just the past events"""
        events = [(ev, ev.prev_occurance()) for ev in self]
        return sorted([r for r in events if r[1]], key=lambda r: r[1].start, reverse=True)

    def current_events(self):
        """Return just the current events"""
        events = [(ev, ev.current_occurance()) for ev in self]
        return sorted([r for r in events if r[1]], key=lambda r: r[1].start)

class Event(Model):
    """A single event"""
    team = ForeignKey(Team, on_delete=SET_NULL, related_name='events', null=True)
    creator = ForeignKey(User, on_delete=SET_NULL, related_name='created_team_events',
                         null=True)
    title = CharField(max_length=255)
    description = TextField(validators=[MaxLengthValidator(4096)])
    recurrences = RecurrenceField(null=True, blank=True)
    start = DateTimeField()
    end = DateTimeField(null=True, blank=True)
    timezone_dst = CharField("DST",
        choices=DST_CHOICES,
        default="UTC",
        max_length=64,
        help_text="Only used for reccuring events, to shift their times"
                  " according to the local daylight savings time. The start"
                  " date/time is always used as the guide for which side of"
                  " the DST the event fell on in the past and thus which side"
                  " it will happen on in future events.")

    link = URLField(null=True, blank=True)
    is_video_link = BooleanField(default=True)

    reminders = CharField(max_length=255, null=True, blank=True,
        help_text="Comma seperated list of minutes prior to meeting when to send out team reminders.")
    remind_all = BooleanField(default=True,
        help_text="If set, includes @all in the message output to alert everyone subscribed.")

    objects = EventQuerySet.as_manager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('calendars:event', kwargs={'pk': self.pk})

    def breadcrumb_parent(self):
        return self.team.events

    def include_rules_text(self):
        for rule in self.recurrences.rrules:
            yield rule.to_text()

    def exlcude_rules_text(self):
        for rule in self.recurrences.exrules:
            yield rule.to_text()

    def get_reminders(self):
        """Parse the reminders and yield each one"""
        next_oc = self.next_occurance()
        if next_oc:
            for x, tdel in enumerate(sorted(self.parse_reminders(), reverse=True)):
                yield x, next_oc.start - tdel

    def parse_reminders(self):
        if self.reminders:
            try:
                return [timedelta(minutes=int(e)) for e in self.reminders.split(",")]
            except Exception:
                pass
        return []

    @property
    def starts(self):
        """Return the start times plus the rrules object"""
        if not hasattr(self, "_soc"):
            self._soc = self.recurrences.occurrences(self.start_tz)
            for exclude, include in self.exceptions.all().flat_starts():
                self._soc.exdate(self.localize(exclude))
                self._soc.rdate(self.localize(include))
        return self._soc

    @property
    def ends(self):
        """Return the end times plus the rrules object"""
        if not hasattr(self, "_eoc"):
            if not self.end_tz:
                return self.starts
            self._eoc = self.recurrences.occurrences(self.end_tz)
            for exclude, include in self.exceptions.all().flat_ends():
                self._eoc.exdate(self.localize(exclude))
                self._eoc.rdate(self.localize(include))
        return self._eoc

    def next_occurance(self):
        """Next start-end datetimes"""
        if self.recurrences:
            start = self.starts.after(now())
            if start:
                end = self.ends.after(start, inc=True)
                return Occurance(start, end)
        elif now() <= self.start:
            return Occurance(self.start, self.end)
        return None

    def prev_occurance(self):
        """Previous start-end datetimes"""
        if self.recurrences:
            end = self.ends.before(now())
            if end:
                start = self.starts.before(end, inc=True)
                return Occurance(start, end)
        elif now() > self.start:
            return Occurance(self.start, self.end)
        return None

    def current_occurance(self):
        """Current event, if happening right now"""
        start = self.start
        end = self.end
        if self.recurrences and end:
            next_end = self.ends.after(now())
            if next_end:
                end = next_end
                start = self.starts.before(end, inc=True)

        if end and (start < now() < end):
            return Occurance(start, end)
        return None

    def next_five(self):
        """Return the next five start times"""
        if self.recurrences:
            for start in self.starts.xafter(now(), count=5):
                end = self.ends.after(start, inc=True)
                yield Occurance(start, end)
        else:
            ocur = self.next_occurance()
            if ocur:
                yield ocur

    @property
    def start_tz(self):
        return self.localize(self.start)

    @property
    def end_tz(self):
        return self.localize(self.end)

    def localize(self, dt):
        if dt is not None:
            return dt.astimezone(self.get_timezone())
        return None

    def get_timezone(self):
        return tz.gettz(self.timezone_dst) or UTC


class ExceptionQuerySet(QuerySet):
    """Manage Exceptions"""
    def flat_starts(self):
        for item in self:
            yield (item.old_start, item.new_start)

    def flat_ends(self):
        for item in self:
            if item.old_end:
                yield (item.old_end, item.new_end)

class EventException(Model):
    """A single exception to a regular event"""
    event = ForeignKey(Event, on_delete=CASCADE, related_name='exceptions')

    old_start = DateTimeField()
    new_start = DateTimeField(null=True, blank=True)
    old_end = DateTimeField(null=True, blank=True)
    new_end = DateTimeField(null=True, blank=True)

    reason = CharField(max_length=128, null=True, blank=True)

    objects = ExceptionQuerySet.as_manager()

    def __str__(self):
        return 'Event moved...'

