#
# Copyright 2023, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Forms for calendars
"""

from dateutil import tz
from datetime import timedelta, datetime

from django.forms import (
    ModelForm, ValidationError,
)
from django.utils.dateparse import parse_date, parse_time
from django.utils.timezone import now

from recurrence import deserialize as deserialize_rec
from djangocms_text_ckeditor.widgets import TextEditorWidget

from .models import UTC, Event, EventException

AUTOCORRECT_WITHIN = timedelta(hours=2)
IGNORE_HISTORY_WITHIN = timedelta(days=7)

class EventForm(ModelForm):
    class Meta:
        model = Event
        exclude = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'description' in self.fields:
            self.fields['description'].widget = TextEditorWidget()

class ExceptionForm(ModelForm):
    class Meta:
        model = EventException
        fields = ['reason', 'old_start', 'new_start', 'old_end', 'new_end']

    def clean_old_start(self):
        return self.clean_old('start')

    def clean_old_end(self):
        return self.clean_old('end')

    def clean_old(self, name):
        old = self.cleaned_data[f'old_{name}']
        nearest = self.get_valid_old(name, old)
        if nearest:
            if abs(nearest - old) > AUTOCORRECT_WITHIN:
                raise ValidationError(f"Bad {name} date, check timezone. Nearest valid {name} date: {nearest}")
            return nearest
        return old

    def get_valid_old(self, name, dt):
        # Ignore unset or past datetimes for record keeping
        if not dt or dt < now() - IGNORE_HISTORY_WITHIN:
            return None

        rec = self.data.get('recurrences', None)
        if not rec:
            raise ValidationError("No recurrences to make exceptions in.")

        dst = tz.gettz(self.data.get('timezone_dst', None)) or UTC
        dt = dt.astimezone(dst)
        try:
            original = datetime.combine(
                parse_date(self.data.get(f'{name}_0', None)),
                parse_time(self.data.get(f'{name}_1', None))
            ).astimezone(dst)
        except (KeyError, ValueError):
            return None

        ocr = deserialize_rec(rec, False).occurrences(original)
        past = ocr.after(dt, inc=True)
        future = ocr.before(dt, inc=True)

        # If there's an exact match, this is perfect
        if past == dt or future == dt:
            return None

        # Return the smallest alternative
        if abs(past - dt) < abs(future - dt):
            return past.astimezone(UTC)
        return future.astimezone(UTC)
