#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Admin for website stats (mostly debugging)
"""

from django.contrib.admin import ModelAdmin, register

from .models import (
    GlobalAccess, LanguageAccess, CountryAccess, SystemAccess, BrowserAccess,
    ApplicationAccess, ReleaseAccess, PageAccess, CachingAccess,
)

@register(GlobalAccess)
class GlobalAccessAdmin(ModelAdmin):
    list_display = ('cadence', 'category', 'count')
    list_filter = ('cadence', 'category',)

@register(LanguageAccess)
class LanguageAccessAdmin(ModelAdmin):
    list_display = ('cadence', 'language', 'count')
    list_filter = ('cadence', 'language',)

@register(CountryAccess)
class CountryAccessAdmin(ModelAdmin):
    list_display = ('cadence', 'country', 'count')
    list_filter = ('cadence', 'country',)

@register(SystemAccess)
class SystemAccessAdmin(ModelAdmin):
    list_display = ('cadence', 'os', 'version', 'count')
    list_filter = ('cadence', 'os',)

@register(BrowserAccess)
class BrowserAccessAdmin(ModelAdmin):
    list_display = ('cadence', 'browser', 'count')
    list_filter = ('cadence', 'browser',)

@register(ReleaseAccess)
class ReleaseAccessAdmin(ModelAdmin):
    list_display = ('cadence', 'release', 'os', 'count')
    list_filter = ('os', 'release')

@register(ApplicationAccess)
class AppAccessAdmin(ModelAdmin):
    list_display = ('cadence', 'application', 'count')
    list_filter = ('cadence', 'application',)

@register(PageAccess)
class PageAccessAdmin(ModelAdmin):
    list_display = ('cadence', 'page', 'count')
    list_filter = ('cadence',)
    search_fields = ('page',)

@register(CachingAccess)
class cachingAccessAdmin(ModelAdmin):
    list_display = ('cadence', 'route', 'download', 'network', 'partial_count', 'complete_count', 'size')
    list_filter = ('cadence', 'route', 'network')
    search_fields = ('download',)
